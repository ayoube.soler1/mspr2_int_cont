//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('./../index');
// let users = require('./../')
let should = chai.should();


chai.use(chaiHttp);

//Our parent block
describe('Books', async () => {
 /*
  * Test the /GET route
  */
  describe('todo projet', () => {
	  it('add todo', (done) => {
			chai.request(server)
		    .get('/user/addTodo')
		    .end((err, res) => {
			  	// res.should.have.status(200);
			  	// res.body.should.be.a('object');
          res.body.status.should.equal('ok', 'ok');
		      done();
		    });

	  });

  });

    // calling home page api
   it('delete one todo', (done) => {
			chai.request(server)
		    .get('/user/removeTodo')
		    .end((err, res) => {
			  	res.should.have.status(200);
          res.body.status.should.equal('ok', 'ok');
		      done();
		    });
	  });
    
}
)