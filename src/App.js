import React, { Component } from 'react';
import uuid from 'uuid/v4'
import axios from 'axios'
import Todos from './Todos';
import TodoForm from './TodoForm'

import {Col} from 'react-bootstrap'

import './App.css';

class App extends Component {
  
  constructor(props) {
    super(props);
  
    this.state = {
      todos: [],
      id: ""
    };
  }

  componentDidMount() {
    this.setState({
      todos: [
        
      ]
    })
  }

  deleteItem(id){
    let newTodos = [];
    axios.get('http://127.0.0.1:3001/user/removeTodo?id=' + id).then((res) => {
      console.log(res)
    })
    newTodos = this.state.todos.filter( ( todo )  => { return todo.id !== id }).map( (todo) => {
      return todo
    })

    this.setState({
      todos: newTodos
    })
  }

  submitTodo(text){
    let id = uuid()
    axios.get('http://127.0.0.1:3001/user/addTodo?todo=' + text + "&id=" + id).then((res) => {
       console.log(res)
      let newTodos = this.state.todos.slice(); // Slice permet de ne pas muter le state (même si on n'utilise pas Redux ici)

      newTodos.push({
        text, 
        id: id
      })

      this.setState({
        todos : newTodos
      })
    })
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Welcome !</h2>
        </div>

        <Col md={8} mdOffset={2}>
        <TodoForm onSubmitForm={(text) => this.submitTodo(text) } />

        <hr/>

        <Todos todos={this.state.todos} onDeleteItem={ (id) => this.deleteItem(id) } />
        </Col>
      </div>
    );
  }
}

export default App;
